const app = require('express')()
const logger = require('morgan')
const bodyParser = require('body-parser')
const cors = require('cors')

const productRoutes = require('./modules/product/routes')
const menuRoutes = require('./modules/menu/routes')

app.use(logger('dev'))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())

app.get('/health', (req, res) => res.send({ health: true }))

app.use('/product', productRoutes)
app.use('/menu', menuRoutes)

app.use((req, res, next) => {
  const err = new Error('URL not found')
  err.status = 404
  next(err)
})
app.use((err, req, res) => {
  res.status(err.status || 500)
  res.send({ error: { message: err.message } })
})

module.exports = app
