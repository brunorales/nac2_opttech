const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
  try {
    const decoded = jwt.verify((req.headers.authorization).split(' ')[1], process.env.JWT_KEY)
    req.id = decoded.id

    next()
  } catch (e) {
    return res.status(401).send({ error: e.message })
  }
}