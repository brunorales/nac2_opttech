const Menu = require('./model')

const getEntries = async (req, res) => {
  try {
    const { entries } = await Menu.findOne()
    return res.status(200).send({ entries })
  } catch (e) {
    return res.status(400).send({ error: e.message })
  }
}

module.exports = {
  getEntries
}
