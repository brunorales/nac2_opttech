const router = require('express').Router()

const menu = require('./controller')

router.get('/entries', menu.getEntries)

module.exports = router
