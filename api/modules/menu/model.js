require('rootpath')()
const mongoose = require('config/db/index')

const menuSchema = new mongoose.Schema({
  entries: []
}, { timestamps: true, collection: 'menu' })

module.exports = mongoose.model('Menu', menuSchema)
