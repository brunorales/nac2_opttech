const router = require('express').Router()

const product = require('./controller')

router.get('/', product.getAll)

module.exports = router