const Product = require('./model')

const getAll = async (req, res) => {
  try {
    return res.status(200).send({ products: await Product.find() })
  } catch (e) {
    return res.status(400).send({ error: e.message })
  }
}

module.exports = {
  getAll
}
