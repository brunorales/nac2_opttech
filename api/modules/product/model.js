require('rootpath')()
const mongoose = require('config/db/index')

const productSchema = new mongoose.Schema({
  name: { type: String, required: true },
  price: { type: Number, required: true },
  description: { type: String, required: true },
  image_path: { type: String, required: true }
}, { timestamps: true, collection: 'products' })

module.exports = mongoose.model('Product', productSchema)
