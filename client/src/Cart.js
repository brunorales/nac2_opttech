import React, { Component } from 'react';
import Menu from './components/menu/Menu';
import './styles/scss/App.scss';
import './styles/scss/Showcase.scss';
import Cart from "./components/cart/Cart";
import store from './store'

class CartPage extends Component {
  render() {
    return (
        <div>
          <Menu/>
          <Cart store={store}/>
        </div>
    );
  }
}

export default CartPage;
