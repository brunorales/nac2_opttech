import { createStore } from 'redux'

const initialState = {
  count: 0,
  products:[]
}

const reducer = (state = initialState, action) => {
  console.log('reducer running', action)

  switch (action.type) {
    case 'ADD':
      const array = state.products
      array.push(action.product)
      const x =  Object.assign({}, state, {
        count: array.length,
        products: array
      })
      localStorage.setItem('cart', JSON.stringify(x))
      return x
    case 'CLEAR':
      const y = Object.assign({}, null, { count: 0, products: [] })
      localStorage.setItem('cart', JSON.stringify(y))
      return y
    default:
      try {
        return JSON.parse(localStorage.getItem('cart'))
      } catch (e) {
        return initialState
      }
  }
  return state
}

const store = createStore(reducer)

export default store
