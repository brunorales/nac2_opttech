import React, { Component } from 'react';
import Button from './components/button/Button';
import Showcase from './components/product/showcase_product/Showcase';
import Menu from './components/menu/Menu';
import store from './../src/store'

import './styles/scss/App.scss';
import './styles/scss/Showcase.scss';
import axios from 'axios'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      products: []
    }
  }

  async componentDidMount() {
    const { data } = await axios({
      url: 'http://localhost:8080/api/product/',
      method: 'get'
    })

    this.setState({
      products: data.products
    })
  }

  renderProducts = () => {
    let products = []
    for(let p of this.state.products) {
      products.push(<Showcase store={store} product={p}/>)
    }
    return products
  }

  render() {
    return (
        <div>
          <Menu/>
          <div className={'showcase'}>
            {this.renderProducts()}
          </div>
        </div>
    );
  }
}

export default App;
