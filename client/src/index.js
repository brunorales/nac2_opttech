import React from 'react';
import ReactDOM from 'react-dom';
import './styles/scss/index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import CartPage from "./Cart";

ReactDOM.render(
    <BrowserRouter>
      <Switch>
        <Route path={'/'} exact={true} component={App}/>
        <Route path={'/cart'} exact={true} component={CartPage}/>
      </Switch>
    </BrowserRouter>
    ,document.getElementById('root'));

serviceWorker.unregister();
