import React from 'react';
import '../../../styles/scss/Showcase.scss';
import { connect } from 'react-redux'

class Showcase extends React.Component{
    constructor(props){
        super(props);
        console.log(props)
        this.state = {
            src: props.product.image_path,
            alt: props.product.name,
            title: props.product.name,
            name: props.product.name,
            value: props.product.price
        }
    }
    render(){
        return(
            <div className={'showcaseProduct'}>
                <div className={'showcaseImage'}>
                    <img className={'img-showcase'} src={this.state.src} alt={this.state.alt} title={this.state.title}/>
                </div>
                <div className={'productName'}>
                    <a href="#">{this.state.name}</a>
                </div>
                <div>
                    <span>R$ {this.state.value}</span>
                </div>
              <button onClick={this.props.onAddToCart}>Adicionar ao carrinho</button>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    onAddToCart: () => {
      console.log('clicking')
      const action = {
        type: 'ADD',
        product: {
          name: ownProps.product.name,
          value: ownProps.product.price,
          src: ownProps.product.image_path,
          qtd: 1
        }
      }
      dispatch(action)
    }
  }
}
export default connect(null, mapDispatchToProps)(Showcase);
