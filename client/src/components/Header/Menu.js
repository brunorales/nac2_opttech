import React, { Component } from "react"
import { connect } from "react-redux"
import PropTypes from "prop-types"
import Icons from './Icons'

class Menu extends Component {

  render() {
    return (
        <div>
          <div className='wrap_menu'>
            <nav className="menu">
              <ul className="main_menu">
                <li><a href="#">Home</a></li>
                <li><a href="#">Shop</a></li>
                <li><a href="#">Blog</a></li>
                <li><a href="#">Sobre</a></li>
                <li><a href="#">Contato</a></li>
              </ul>
            </nav>
          </div>

          <Icons/>
        </div>
    )
  }
}
export default Menu
