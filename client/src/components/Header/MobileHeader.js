import React, { Component } from "react"
import { connect } from "react-redux"
import PropTypes from "prop-types"

class MobileHeader extends Component {

  render() {
    return (
        <div className="wrap_header_mobile">
          <a href="index.html" className="logo-mobile">
            <img src="images/icons/logo.png" alt="IMG-LOGO" />
          </a>

          <div className="btn-show-menu">
            <div className="header-icons-mobile">
              <a href="#" className="header-wrapicon1 dis-block">
                <img src="images/icons/icon-header-01.png" className="header-icon1" alt="ICON" />
              </a>

              <span className="linedivide2"></span>

              <div className="header-wrapicon2">
                <img src="images/icons/icon-header-02.png" className="header-icon1 js-show-header-dropdown" alt="ICON" />
                  <span className="header-icons-noti">0</span>
              </div>
            </div>

            <div className="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span className="hamburger-box">
						<span className="hamburger-inner"></span>
					</span>
            </div>
          </div>
        </div>
    )
  }
}
export default MobileHeader;
