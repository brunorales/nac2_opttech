import React, { Component } from "react"
import { connect } from "react-redux"
import PropTypes from "prop-types"
import Menu from "./Menu";
import Icons from "./Icons";
import MobileHeader from "./MobileHeader";

class Header extends Component {

  render() {
    return (
        <div className='header1'>
          <div className='container-menu-header'>
            <div className='wrap_header'>
              <a href='#' className='logo'>
                <img src="assets/images/logo.svg" alt=".Store"/>
              </a>
            </div>
            <Menu/>
          </div>
          <MobileHeader/>
        </div>
    )
  }
}
export default Header;
