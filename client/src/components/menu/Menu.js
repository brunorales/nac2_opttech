import React from 'react';
import '../../styles/scss/Menu.scss'
import axios from "axios";
import store from './../../store'
import MySpan from "../cart/Counter";


class List extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return(
        <li><a style={{color: '#000', textDecoration: 'none',}} href={'/'}>{this.props.name}</a></li>
    )
  }
}

class Menu extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            entries: [],
            srcLogo: './assets/img/logo.jpeg',
            titleLogo: 'Logo',
            altLogo: 'Logo',
            srcBag: './assets/icons/bag.png',
            altBag: 'Minhas Compras',
            titleBag: 'Minhas Compras',
        }
    }

  async componentDidMount() {
    const { data } = await axios({
      url: 'http://localhost:8080/api/menu/entries',
      method: 'get'
    })

    this.setState({ entries: data.entries })
  }

  createList = () => {
    let list = []

    console.log(this.state.entries)

    for(let e of this.state.entries) {
      list.push(<List name={e} />)
    }

    return list
  }


    render(){
        return(
            <div className={'menu'}>
                <div className={'logo'}>
                    <img src={this.state.srcLogo} alt={this.state.altLogo} title={this.state.titleLogo}/>
                </div>
                <div className={'menuList'}>
                    <nav>
                        <ul>
                          { this.createList() }
                        </ul>
                    </nav>
                </div>
                <div className={'bag'}>
                  <a href={'/cart'}><img src={this.state.srcBag} alt={this.state.altBag} title={this.state.titleBag} />
                    <MySpan store={store}/>
                  </a>
                </div>

            </div>
        );
    }
}

export default Menu;
