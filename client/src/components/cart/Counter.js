import React from "react";
import { connect } from 'react-redux'

function MySpan(props) {
  return(
      <span>{props.count}</span>
  )
}
function mapStateToProps(state) {
  console.log('mapStateToProps', state)
  return {
    count: state.count
  }
}

export default connect(mapStateToProps)(MySpan)
