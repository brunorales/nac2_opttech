import React from 'react';
import Input from '../../input/Input';
import Button from '../../button/Button';
import '../../../styles/scss/Checkout.scss';


class Checkout extends  React.Component{
    constructor(props){
        super(props);
        this.state ={
            title: 'Total da Compra',
            subtotal: 75,
            optionValue: 'Brasil',
            total: 75
        }
    }
    render(){
        return(
            <div className={'itemCheckout'}>
                <h5>{this.state.title}</h5>
                <div className={'left'}>
                    <span className={'subtotal'}>Subtotal:</span>
                    <span className={'send'}>Envio:</span>

                </div>
                <div className={'right'}>
                    <span className={'subtotal'}>R$ {this.state.subtotal}</span>
                    <span className={'send'}>CALCULAR ENVIO
                        <select>
                            <option value={this.state.optionValue}>{this.state.optionValue}</option>
                        </select>
                    </span>
                    <Input name="estado" placeholder="Estado"/>
                    <Input name="cep" placeholder="CEP" />
                    <Button name="ATUALIZAR"/>

                </div>
                <div className={'total'}>
                    <span className={'subtotal'}>Total:</span>
                    <span className={'send'}><strong>R$ {this.state.total}</strong></span>
                </div>
                <Button name="FINALIZAR COMPRA" />
            </div>
        );
    }
}

export default Checkout;
