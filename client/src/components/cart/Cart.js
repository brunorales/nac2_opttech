import React from 'react';
import CartItem from './cart_item/CartItem';
import Button from '../button/Button';
import Checkout from './checkout/Checkout';
import Input from '../input/Input';

import '../../styles/scss/Cart.scss';
import { connect } from 'react-redux'
import store from "../../store";
import Icons from "../Header/Icons";

class Cart extends React.Component {
    constructor(props){
        super(props);

    }

    getItems = () => {
      let items = []
      console.log(this.props.products)
      for(let p of this.props.products) {
        items.push(<CartItem store={store} product={p} />)
      }
      return items
    }

    render(){
        return(
            <div>
              <button onClick={this.props.clearCart}>Limpar</button>
              {this.getItems()}
                <div className={"cart"}>
                    <div className={'cartForm'}>
                        <form action="POST">
                            <Input name="cupom" placeholder="Insira o número do cupom" />
                            <Button name="APLICAR CUPOM" />
                            <Button name="ATUALIZAR" />
                        </form>
                    </div>
                </div>
                <div className={'checkout'}>
                    <Checkout />
                </div>

            </div>


        );
    }
}

function mapStateToProps(state) {
  console.log('mapStateToProps', state)
  return {
    products: state.products
  }
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    clearCart: () => {
      console.log('clicking')
      const action = {
        type: 'CLEAR',
      }
      dispatch(action)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
