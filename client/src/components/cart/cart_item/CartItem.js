import React from 'react';
import Input from '../../input/Input';

import '../../../styles/scss/Cartitem.scss'

class CartItem extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            productName: props.product.name,
            value: props.product.value,
            quantity: props.product.qtd,
            total: props.product.value,
            src: props.product.src
        }
    }
    render(){
        return(
            <div className={'cartItem'}>
                  <div className={'cartTable'}>
                    <table>
                        <thead>
                        <tr>
                            <th></th>
                            <th>Produto</th>
                            <th>Preço</th>
                            <th>Quantidade</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><img src={this.state.src}/></td>
                            <td>{this.state.productName}</td>
                            <td>R$ {this.state.value}</td>
                            <td><Input name="quantidade" placeholder="0"/></td>
                            <td><strong>R$ {this.state.total}</strong></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        );
    }
}

export default CartItem;
