import React from 'react';
import '../../styles/scss/Input.scss';

class Input extends React.Component{
    render(){
        return(
            <input type="text" name={this.props.name} placeholder={this.props.placeholder} />
        );
    }
}

export default Input;