import React from 'react';
import '../../styles/scss/Button.scss'

class Button extends React.Component{
    render(){
        return(
            <button type="button" className={'button'}>{this.props.name}</button>
        );
    }
}

export default Button;


