
# NAC 2 : Optimization Technology 

Projeto feito para a disciplina de Optimization Technology da FIAP.

## Execução

### Requisitos

Ter instalado:

```
docker
docker-compose
```

### Passo-a-passo

Siga os passos detalhadamente para executar o projeto.

**1 -> Faça um clone do projeto e entre nela no terminal**

```
~ git clone https://github.com/brunorales/nac2_opttech.git
~ cd nac2_opttech
```

**2 -> Instale as dependências de da api e client**

Instale as dependências dos dois projetos. Você pode fazer esta etapa manualmente ou execute o script contido:
```
~ chmod +x install_nm.sh
~ ./install_nm.sh
```

**3 -> Execute o orquestrador de containers para subir o ambiente**
```
~ docker-compose up --build
```
*OBS.:* Pode ser que para executar esta etapa você precise de permissão elevada. Apenas rode o mesmo comando com `sudo` .
Esta etapa levará um tempo para baixar as imagens do docker-hub.

**4 -> Repopular o banco de dados**

Um banco zerado será criado dentro do diretório do ambiente, execute o seguinte comando para populá-lo.
```
~ mongorestore -d nac2 db_backup/nac2
```
*OBS.:* Pode ser que para executar esta etapa você precise de permissão elevada. Apenas rode o mesmo comando com `sudo` .


## Integrantes

* **Bruno Morales** - 78473
* **Cirilo Souza** - 78684
* **Marcos Vaz** - 78856
* **João Miguel** - 76671
* **Fernando Meyer** - 77786
